// Lesson_16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#define __STDC_WANT_LIB_EXT1__ 1
#define _XOPEN_SOURCE
#include <ctime>
#include <iostream>

using namespace std;

int main()
{
    const int n1 = 5;
    const int n2 = n1;
    int array [n1][n2];
    
    for (int i = 0; i < n1; i++)
    { 
        for (int j = 0; j < n2; j++)
        {
            array[i][j] = (i+j);
            cout << array[i][j] << ' ';
        }
        cout << "\n";
    }

    int sum = 0;

    const time_t t0 = time(NULL); // �-� time ���������� ������� � 00:00 1 ������ 1970 � time_t �������
    tm t1;
    localtime_s(&t1, &t0); // ������� ������ time_t � ������ tm � ������� � - �� localtime 
    /* 
    * errno_t localtime_s (struct tm * const tmDest, time_t const * const sourceTime)
    * tmDest ��� ��������� �� ��������� �������, ������� ��������� ��������� (t1)
    * sourceTime ��� ��������� �� �������� �����. (t0)
    */
    int date = t1.tm_mday;
    int i = date % n1;

    for (int j = 0; j < n1; j++)
    {
        sum += array[i][j];
    }
    cout << endl << sum << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
